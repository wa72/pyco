

## Tasks

Immer vom lokalen Projekt aus.

### clone push

Grund/Zweck:

- Lokales Projekt ist fertig. Erstes deploy auf den Server.
- Umzug des Projekts auf neuen Server.

Handlung:

- Datenbank übertragen
- extensions
- fileadmin
- uploads

### clone pull

Grund/Zweck:

- Lokales Projekt defekt/leer. Neues lokales Projekt auf anderem Entwicklungsrechner
- Umzug des Projekts auf neuen Server.


Handlung:

- Datenbank übertragen
- extensions
- fileadmin
- uploads

### push

Grund/Zweck:

- Änderungen im site package

Handlung:
- site package
- sonstige extensions


## Konfiguration

If you use composer installation with ``public/`` folder (default) you need to set in your deploy.php:
::

      set('web_path', 'public/');


In deploy.php set the folders you want to synchronize:
::

      set('media',
          [
           'filter' => [
               '+ /fileadmin/',
               '- /fileadmin/_processed_/*',
               '+ /fileadmin/**',
               '+ /uploads/',
               '+ /uploads/**',
               '- *'
          ]
      ]);


## Different folder structures
### New default: all in vendor

```bash
.
├── config
│   └── sites
├── packages
│   └── mysitepackage
├── public
│   ├── _assets
│   ├── fileadmin
│   ├── typo3
│   ├── typo3conf
│   ├── typo3temp
│   └── uploads
├── var
│   ├── cache
│   ├── charset
│   ├── labels
│   ├── lock
│   ├── log
│   ├── session
│   └── transient
└── vendor
    ├── symfony
    ├── typo3
    └── wa72
```

Wenn das target eine Symlink-Installation ist:
```bash
rsync -rptgo -v  ./vendor/wa72/sitepackage/ p1234@p1234.de:html/typo3/typo3conf/ext/mysitepackage/
```
Wenn das target eine Vendor-Installation ist bei Mittwald:
```bash
rsync -rptgo -v  ./vendor/wa72/sitepackage/ p1234@p1234.de:html/typo3-composer/vendor/wa72/sitepackage/
```
**Wichtig: die Slashes am Ende!**



### Old symlink installation

1. Ganz altes System: fileadmin/wa72/
2. Sitepackage: user_site
3. Sitepackage: mysitepackage

```bash
.
└── htdocs
    ├── fileadmin
    │   ├── form_definitions
    │   ├── user_upload
    │   ├── user_site
    │   └── wa72
    │       ├── Configuration
    │       ├── Resources
    │       ├── base
    │       └── local
    ├── typo3 -> typo3_src/typo3
    ├── typo3_src -> /var/www/_typo3_src/typo3_src-10
    ├── typo3conf
    │   ├── autoload
    │   ├── ext
    │   │   └── user_site
    │   └── sites
    │       └── main
    └── uploads
```

### Example pyco.php for composer installation
```php
<?php declare(strict_types=1);

namespace Wa72\Pyco;

use Wa72\Pyco\Configuration\Configuration;
use Wa72\Pyco\Task\Context;
use function Wa72\HelperCollection\merge_paths;

require_once 'vendor/wa72/pyco/src/sets/default/typo3v11.php';

Configuration::set('site_package_name', 'mysitepackage');

$hostAlias = 'localhost';
host($hostAlias)
    ->set('web_path', 'tmp/public')
    ->set('db_storage_path', '/tmp')
    ->add('database',
        [
            'dbname'   => 'mylocaldb',
            'user'     => 'dbuser',
            'password' => 'dbpassword',
            'host'     => 'localhost',
        ]
    );


// modern composer installation
host($hostAlias)
    ->set('site_package_path', merge_paths( 'packages', get('site_package_name')));

// example: Mittwald composer installation
$hostAlias = 't3composer';
host($hostAlias)
    ->set('hostname', 'p000000.mittwaldserver.info')
    ->set('remote_user', 'p000000')
    ->set('deploy_path', '/html/typo3-composer')
    ->set('web_path', 'public')
    ->set('db_storage_path', '/files')
    ->set('local/bin/mysqldump', '/usr/local/bin/mysqldump')
    ->set('local/bin/mysql', '/usr/local/bin/mysql')
    ->set('local/bin/gzip', '/bin/gzip')
    ->add('database', [
        'dbname'   => 'usr_p000000_1',
        'user'     => 'p000000',
        'password' => 'p000000password',
        'host'     => 'db0000.mydbserver.com',
    ]);

Context::activate(host($hostAlias));
host($hostAlias)
    ->set('site_package_path', merge_paths(get('deploy_path'), 'vendor/my/sitepackage'));
Context::clear();
```

### Example pyco.php for classic typo3conf/ext installation
```php
<?php declare(strict_types=1);

namespace Wa72\Pyco;

use Wa72\Pyco\Configuration\Configuration;
use Wa72\Pyco\Task\Context;
use function Wa72\HelperCollection\merge_paths;

require_once 'vendor/wa72/pyco/src/sets/default/typo3v11.php';

Configuration::set('site_package_name', 'mysitepackage');

$hostAlias = 'localhost';
host($hostAlias)
    ->set('web_path', 'htdocs')
    ->set('db_storage_path', '/tmp')
    ->add('database',
        [
            'dbname'   => 'mylocaldb',
            'user'     => 'dbuser',
            'password' => 'dbpassword',
            'host'     => 'localhost',
        ]
    );

Context::activate(host($hostAlias));
host($hostAlias)
    ->set('site_package_path', merge_paths(get('deploy_path'), 'typo3conf/ext', get('site_package_name')) );
Context::clear();


// example: Mittwald classic installation
$hostAlias = 't3composer';
host($hostAlias)
    ->set('hostname', 'p000000.mittwaldserver.info')
    ->set('remote_user', 'p000000')
    ->set('deploy_path', '/html/typo3')
    ->set('web_path', '')
    ->set('db_storage_path', '/files')
    ->set('local/bin/mysqldump', '/usr/local/bin/mysqldump')
    ->set('local/bin/mysql', '/usr/local/bin/mysql')
    ->set('local/bin/gzip', '/bin/gzip')
    ->add('database', [
        'dbname'   => 'usr_p000000_1',
        'user'     => 'p000000',
        'password' => 'p000000password',
        'host'     => 'db0000.mydbserver.com',
    ]);

Context::activate(host($hostAlias));
host($hostAlias)
    ->set('site_package_path', merge_paths(get('deploy_path'), 'typo3conf/ext', get('site_package_name')) );
Context::clear();
```

## Filese & Folders
set-media.php
exclude: .git

set-0.php
filter

set: typo3v11
'shared_dirs'
