<?php

namespace Wa72\Pyco;

use function Wa72\HelperCollection\merge_paths;

set('shared_dirs', function () {
    return [
        merge_paths(get('web_path'), 'fileadmin'),
        merge_paths(get('web_path'), 'uploads'),
        merge_paths(get('web_path'), 'typo3temp/assets/_processed_'),
        merge_paths(get('web_path'), 'typo3temp/assets/images'),
        !empty(get('web_path')) ? 'var/log' : 'typo3temp/var/log',
        !empty(get('web_path')) ? 'var/transient' : 'typo3temp/var/transient',
    ];
});

// Look on https://github.com/sourcebroker/deployer-extended#buffer-start for docs
set('buffer_config', function () {
    return [
        'index.php'         => [
            'entrypoint_filename' => get('web_path') . 'index.php',
        ],
        'typo3/index.php'   => [
            'entrypoint_filename' => get('web_path') . 'typo3/index.php',
        ],
        'typo3/install.php' => [
            'entrypoint_filename' => get('web_path') . 'typo3/install.php',
        ]
    ];
});

// Look https://github.com/sourcebroker/deployer-extended-database for docs
add('db_default', [
//    'port'     => 3306,
    'truncate_tables'     => [
        // Do not truncate caching tables "cache_imagesizes" as the image settings are not changed frequently and regenerating images is processor extensive.
        '(?!cache_imagesizes)cache_.*',
    ],
    'ignore_tables_out'   => [
// pyco doesn't support wildcard configuration
// reason: no connection to foreign host database to use $databaseUtility->getTables() and filter them out.
//        'cf_.*',
//        'cache_.*',
        'cf_cache_hash',
        'cf_cache_hash_tags',
        'cf_cache_imagesizes',
        'cf_cache_imagesizes_tags',
        'cf_cache_pages',
        'cf_cache_pages_tags',
        'cf_cache_pagesection',
        'cf_cache_pagesection_tags',
        'cf_cache_rootline',
        'cf_cache_rootline_tags',
        'cf_cache_ttaddress_category',
        'cf_cache_ttaddress_category_tags',
        'cf_extbase_datamapfactory_datamap',
        'cf_extbase_datamapfactory_datamap_tags',
        'cf_extbase_object',
        'cf_extbase_object_tags',
        'cf_extbase_reflection',
        'cf_extbase_reflection_tags',
        'cf_extbase_typo3dbbackend_queries',
        'cf_extbase_typo3dbbackend_queries_tags',
        'cf_ttaddress_geocoding',
        'cf_ttaddress_geocoding_tags',
        'cache_hash',
        'cache_hash_tags',
        'cache_imagesizes',
        'cache_imagesizes_tags',
        'cache_md5params',
        'cache_pages',
        'cache_pages_tags',
        'cache_pagesection',
        'cache_pagesection_tags',
        'cache_rootline',
        'cache_rootline_tags',
        'cache_sys_dmail_stat',
        'cache_treelist',

        'be_sessions',
        'fe_sessions',
        'fe_session_data',
        'sys_file_processedfile',
        'sys_history',
        'sys_log',
        'sys_refindex',
        'tx_devlog',
        'tx_extensionmanager_domain_model_extension',
        'tx_powermail_domain_model_mail',
        'tx_powermail_domain_model_answer',
        'tx_solr_.*',
        'tx_crawler_queue',
        'tx_crawler_process',
    ],
    'post_sql_in'         => '',
    'post_sql_in_markers' => ''
]);

// Look https://github.com/sourcebroker/deployer-extended-database for docs
set('db_databases',
    [
        'database_default' => [
            get('db_default'),
            function () {
                if (get('driver_typo3cms', false)) {
                    return (new \SourceBroker\DeployerExtendedTypo3\Drivers\Typo3CmsDriver)->getDatabaseConfig();
                }

                return !empty($_ENV['IS_DDEV_PROJECT']) ? get('db_ddev_database_config') :
                    (new \SourceBroker\DeployerExtendedTypo3\Drivers\Typo3EnvDriver)->getDatabaseConfig(
                        [
                            'host'     => 'TYPO3__DB__Connections__Default__host',
                            'port'     => 'TYPO3__DB__Connections__Default__port',
                            'dbname'   => 'TYPO3__DB__Connections__Default__dbname',
                            'user'     => 'TYPO3__DB__Connections__Default__user',
                            'password' => 'TYPO3__DB__Connections__Default__password',
                        ]
                    );
            }
        ]
    ]
);
