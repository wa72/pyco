<?php
/**
 * settings from pystanium.sh
 */

use function Wa72\Pyco\get;
use function Wa72\Pyco\set;

// -avu --checksum --copy-links --keep-dirlinks --delete --info=progress2'
set('media_rsync_flags', '-avucLK --safe-links --delete --info=progress2');

//set('media', function () {
//    return [
//        'filter' => [
//
//        ]
//    ];
//});
