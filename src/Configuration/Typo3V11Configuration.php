<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 10.03.2022
 * Time: 09:12
 */

namespace Wa72\Pyco\Configuration;

use Wa72\HelperCollection\FilesystemHelper;

class Typo3V11Configuration
{
    public array $s = [];

    public function init(array $defaults)
    {
        $webpath = $defaults['web_path'];
        $this->s['shared_dirs'] = [
            merge_paths($webpath, 'fileadmin'),
            merge_paths($webpath, 'uploads'),
            merge_paths($webpath, 'typo3temp/assets/_processed_'),
            merge_paths($webpath, 'typo3temp/assets/images'),
//            !empty(get('web_path')) ? 'var/log' : 'typo3temp/var/log',
//            !empty(get('web_path')) ? 'var/transient' : 'typo3temp/var/transient',
        ];


        $this->s['truncate_tables'] = [
            // Do not truncate caching tables "cache_imagesizes" as the image settings are not changed frequently
            // and regenerating images is processor extensive.
            '(?!cache_imagesizes)cache_.*',
        ];

        $this->s['ignore_tables_out'] = [
            'cf_.*',
            'cache_.*',
            'be_sessions',
            'fe_sessions',
            'fe_session_data',
            'sys_file_processedfile',
            'sys_history',
            'sys_log',
            'sys_refindex',
            'tx_devlog',
            'tx_extensionmanager_domain_model_extension',
            'tx_powermail_domain_model_mail',
            'tx_powermail_domain_model_answer',
            'tx_solr_.*',
            'tx_crawler_queue',
            'tx_crawler_process',
        ];

        $this->s = array_replace_recursive($this->s, $defaults);
    }


}
