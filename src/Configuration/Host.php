<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 12.03.2022
 * Time: 15:40
 */

namespace Wa72\Pyco\Configuration;

class Host
{
    use ConfigurationAccessor;

    private string $alias;

    private string $hostname;
    private string $realHostname;
    private string $user;
    private string $port;
//    private $configFile;
    private string $identityFile;
    private bool $forwardAgent = true;
    private ?bool $multiplexing = null;
//    private $sshArguments;
    private string $shellCommand = 'bash -s';

    /** @var Host[] $hosts */
    public static array $hosts = [];

    public static function host(string $hostname):Host {
        if(isset(self::$hosts[$hostname])) {
            return self::$hosts[$hostname];
        }

        $host = new Host($hostname);
        self::$hosts[$hostname] = $host;

        return $host;
    }

    public function __construct(string $alias, ?string $hostname = null)
    {
        $this->alias = $alias;
        $this->set('alias', $alias);

        if(null === $hostname) {
            $hostname = $alias;
        }

        $this->hostname = $hostname;
        $this->setRealHostname($hostname);

        $this->set('hostname', $hostname);
        $this->set('real_hostname', $hostname);

//        $this->sshArguments = new Arguments();
    }

    public function getConfigurationKeyName(): string
    {
        return 'host';
    }

    public function getConfigurationKey(): string
    {
        return $this->alias;
    }


    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return string
     */
    public function getRealHostname(): string
    {
        return $this->realHostname;
    }

    /**
     * @param string $realHostname
     */
    public function setRealHostname(string $realHostname): Host
    {
        $this->set('real_hostname', $realHostname);
        $this->realHostname = $realHostname;

        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port): void
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getIdentityFile(): string
    {
        return $this->identityFile;
    }

    /**
     * @param string $identityFile
     */
    public function setIdentityFile(string $identityFile): void
    {
        $this->identityFile = $identityFile;
    }

    /**
     * @return bool
     */
    public function isForwardAgent(): bool
    {
        return $this->forwardAgent;
    }

    /**
     * @param bool $forwardAgent
     */
    public function setForwardAgent(bool $forwardAgent): void
    {
        $this->forwardAgent = $forwardAgent;
    }

    /**
     * @return bool|null
     */
    public function getMultiplexing(): ?bool
    {
        return $this->multiplexing;
    }

    /**
     * @param bool|null $multiplexing
     */
    public function setMultiplexing(?bool $multiplexing): void
    {
        $this->multiplexing = $multiplexing;
    }

    /**
     * @return string
     */
    public function getShellCommand(): string
    {
        return $this->shellCommand;
    }

    /**
     * @param string $shellCommand
     */
    public function setShellCommand(string $shellCommand): void
    {
        $this->shellCommand = $shellCommand;
    }


}
