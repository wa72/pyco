<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 12.03.2022
 * Time: 15:45
 */

namespace Wa72\Pyco\Configuration;

/**
 * Zugriff auf Configuration.
 * Verwendet jedoch automatisch den ConfigurationKeyName und ConfigurationKey.
 *
 */
trait ConfigurationAccessor
{

    public function getConfigurationKeyName(): ?string {
        return null;
    }

    public abstract function getConfigurationKey(): string;

    public function getMergedWithDefaults(string $name, string $defaultName): array
    {
        return Configuration::getMergedWithDefaults($name, $defaultName, $this->getConfigurationKeyName(), $this->getConfigurationKey());
    }

    public function getConfiguration(): array
    {
        if(null === $this->getConfigurationKeyName()) {
            return Configuration::$values;
        } else {
            return Configuration::$values[$this->getConfigurationKeyName()][$this->getConfigurationKey()];
        }
    }

    /**
     * Get configuration options
     *
     * @param mixed $default
     * @return mixed
     */
    public function get(string $name, $default = null)
    {
        return Configuration::get($name, $default, $this->getConfigurationKeyName(), $this->getConfigurationKey());
    }

    /**
     * Check configuration option
     */
    public function has(string $name): bool
    {
        return Configuration::has($name, $this->getConfigurationKeyName(), $this->getConfigurationKey());
    }

    /**
     * Set configuration option
     *
     * @param mixed $value
     * @return static
     */
    public function set(string $name, $value)
    {
        Configuration::set($name, $value, $this->getConfigurationKeyName(), $this->getConfigurationKey());
        return $this;
    }

    /**
     * Add configuration option
     *
     * @return static
     */
    public function add(string $name, array $value)
    {
        Configuration::add($name, $value, $this->getConfigurationKeyName(), $this->getConfigurationKey());
        return $this;
    }
}
