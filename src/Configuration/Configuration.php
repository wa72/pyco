<?php
/**
 * Simplified combination of:
 * - Deployer\functions.php
 * - Deployer\Collection
 * - Deployer\Configuration
 *
 * You can use the sets from the sourcebroker/deployer-extended-typo3 package.
 *
 * Globale Konfiguration.
 *
 * Z.b.
 * Configuration::set('path', '/tmp')
 * Configuration::get('path')
 *
 * Oder speichern in Unter-Konfigurationen:
 * Configuration::set('url', 'typo3.com', 'project', 'typo3');
 * Configuration::set('type', 'Web CMS', 'project', 'typo3');
 * Configuration::set('url', 'symfony.com', 'project', 'symfony');
 * Configuration::set('type', 'PHP Framework', 'project', 'symfony');
 *
 * ->
 *
 * path            /tmp
 * project
 *    typo3
 *       url       typo3.com
 *       type      Web CMS
 *    symfony
 *       url       symfony.com
 *       type      PHP Framework
 */

namespace Wa72\Pyco\Configuration;

use Wa72\HelperCollection\ArrayHelper;
use Wa72\HelperCollection\GenericHelper;
use function Wa72\Pyco\array_merge_alternate;

class Configuration
{
    /**
     *
     */
    public static array $values = [];

    public static function add(
        string $name,
        array $array,
        ?string $configurationKeyName = null,
        ?string $configurationKey = null
    ) {
        if (self::has($name, $configurationKeyName, $configurationKey)) {
            $config = self::get($name, $configurationKeyName, $configurationKey);
            if (!is_array($config)) {
                $message = "Configuration parameter `$name` isn't array.";
                die($message);
//                throw new \Exception($message);
//                throw new ConfigurationException($message);
            }
            self::set($name, array_merge_alternate($config, $array), $configurationKeyName, $configurationKey);
        } else {
            self::set($name, $array, $configurationKeyName, $configurationKey);
        }
    }

    public static function addDefault(
        string $name,
        array $array
    ) {
        self::add($name, $array, null, null);
    }

    public static function addSet(
        string $name
    ) {
        die('not supported.');
    }

    public static function get(
        string $name,
        $default = null,
        ?string $configurationKeyName = null,
        ?string $configurationKey = null
    ) {
        if (null === $configurationKeyName || null === $configurationKey) {
            $config = self::$values;
        } else {
            $config = self::$values[$configurationKeyName][$configurationKey];
        }

        if (isset($config[$name])) {
            if (GenericHelper::isClosure($config[$name])) {
                $value = $config[$name] = call_user_func($config[$name]);
            } else {
                $value = $config[$name];
            }
        } else {
            // parameter not in Context? look globally...
            $config = self::$values;
            if (isset($config[$name])) {
                if (GenericHelper::isClosure($config[$name])) {
                    $value = call_user_func($config[$name]);
                } else {
                    $value = $config[$name];
                }
            } else {
                if (null === $default) {
                    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

                    $callerInfo = '';
                    for ($i = 0; $i < count($backtrace); $i++) {
                        if (!isset($backtrace[$i]['file']) || '' == $backtrace[$i]['file']) {

                        } else {
                            $callerInfo .= $i . ' ' . $backtrace[$i]['file'] . ':' . $backtrace[$i]['line'] . PHP_EOL;
                        }
                    }

                    $message = "Configuration parameter "
                               . ($configurationKeyName ? "`{$configurationKeyName}`." : '')
                               . ($configurationKey ? "`{$configurationKey}`." : '')
                               . "`$name` does not exist." . PHP_EOL
                               . "Caller:" . PHP_EOL . $callerInfo;

                    $exception = new \Exception($message);
                    throw $exception;
                } else {
                    $value = $default;
                }
            }
        }

        return $value;
    }

    /**
     * @param string $configurationKeyName
     * @param string $name
     * @param string $defaultName
     * @return array
     * @throws \Exception
     */
    public static function getMergedWithDefaults(
        string $name,
        string $defaultName,
        ?string $configurationKeyName = null,
        ?string $configurationKey = null
    ): array {
        if (!isset(self::$values[$defaultName])) {
            $message = "No default configuration '{$defaultName}' found.";
            $message .= PHP_EOL . self::generateCallTrace();

            die($message);
        }

        // possibly eg per host configuration
        $configuration = self::get($name, null, $configurationKeyName, $configurationKey);

        // always use global default settings
        $original = self::$values[$defaultName];
        if (self::has($name)) {
            $override = self::get($name);
        } else {
            $override = $configuration;
        }

        return array_merge_alternate($original, $override);
    }

    public static function has(
        string $name,
        ?string $configurationKeyName = null,
        ?string $configurationKey = null
    ): bool {
        if (null === $configurationKeyName || null === $configurationKey) {
            return array_key_exists($name, self::$values);
        } elseif (!array_key_exists($configurationKeyName, self::$values)) {
            return false;
        } elseif (!array_key_exists($configurationKey, self::$values[$configurationKeyName])) {
            return false;
        } else {
            return array_key_exists($name, self::$values[$configurationKeyName][$configurationKey]);
        }
    }

    public static function set(
        string $name,
        $value,
        ?string $configurationKeyName = null,
        ?string $configurationKey = null
    ) {
        if (null === $configurationKeyName || null === $configurationKey) {
            self::$values[$name] = $value;
        } else {
            self::$values[$configurationKeyName][$configurationKey][$name] = $value;
        }
    }

    public static function printAll()
    {
        try {
            echo ArrayHelper::prettyDump(self::$values) . PHP_EOL;
        } catch (\Exception $e) {
        }
    }

    protected static function generateCallTrace(): string
    {
        $e = new \Exception();
        $trace = explode("\n", $e->getTraceAsString());
        // reverse array to make steps line up chronologically
//        $trace = array_reverse($trace);
        array_shift($trace); // remove {main}
        array_pop($trace); // remove call to this method
        $length = count($trace);
        $result = [];

        for ($i = 0; $i < $length; $i++) {
            $result[] = ($i + 1) . ')' . substr($trace[$i],
                    strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
        }

        return implode("\n", $result);
    }
}
