<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 19.07.2022
 * Time: 16:44
 *
 * Collects commands and executes (outputs) them.
 *
 */

namespace Wa72\Pyco\Executor;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Wa72\HelperCollection\EnvironmentHelper;
use Wa72\Pyco\Task\Command;

class SeriesExecutor
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /** @var Command[] */
    private $cmds = [];

    public function __construct(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @param Command[] $commands
     * @return void
     */
    public function addCommands(array $commands) {
        $this->cmds = array_merge($this->cmds, $commands);
    }

    /**
     * @return void
     */
    public function run() {
        $isDdevProject = EnvironmentHelper::isDdevProject();
// rsync --dry-run -avu --filter='merge .pyco/rsync_filters' --checksum --delete --info=progress2 --copy-links --keep-dirlinks --safe-links p616683@p616683.mittwaldserver.info:"/html/typo3/public/" "public/"
        foreach ($this->cmds as $cmd) {
            $runNotInDdev = false;
            if(str_contains($cmd->getCmd(), 'scp ')) {
                $runNotInDdev = true;
            }

            if ($cmd->isLocal() && $isDdevProject && !$runNotInDdev) {
                $line = 'ddev exec "' .  $cmd->getCmd() . '"';
            } else {
                $line = $cmd->getCmd();
            }

            $this->output->writeln($line);

            $cmd->setHasRun(true);
        }
    }
}
