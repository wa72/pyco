<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 13.03.2022
 * Time: 14:27
 */

namespace Wa72\Pyco\Task;

class Command
{
    private bool $local = false;
    private bool $hasRun = false;
    private string $cmd = '';

    /**
     * @param string $cmd
     */
    public function __construct(string $cmd, bool $local = false)
    {
        $this->cmd = $cmd;
        $this->local = $local;
    }

    /**
     * @return bool
     */
    public function isLocal(): bool
    {
        return $this->local;
    }

    /**
     * @param bool $local
     */
    public function setLocal(bool $local): void
    {
        $this->local = $local;
    }

    /**
     * @return bool
     */
    public function isHasRun(): bool
    {
        return $this->hasRun;
    }

    /**
     * @param bool $hasRun
     */
    public function setHasRun(bool $hasRun): void
    {
        $this->hasRun = $hasRun;
    }

    /**
     * @return string
     */
    public function getCmd(): string
    {
        return $this->cmd;
    }
}
