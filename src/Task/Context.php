<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 12.03.2022
 * Time: 15:35
 */

namespace Wa72\Pyco\Task;

use Wa72\Pyco\Configuration\Host;

class Context
{
    private static ?Host $host = null;

    public static function has(): bool
    {
        return null !== self::$host;
    }

    public static function getHost(): Host
    {
        return self::$host;
    }

    public static function activate(Host $host): void
    {
        self::$host = $host;
    }

    public static function clear(): void
    {
        self::$host = null;
    }
}
