<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 12.03.2022
 * Time: 15:18
 */

namespace Wa72\Pyco\Task;

use Wa72\HelperCollection\ArrayHelper;
use Wa72\Pyco\Configuration\Configuration;
use Wa72\Pyco\Utility\DatabaseUtility;
use function Wa72\HelperCollection\merge_paths;
use function Wa72\Pyco\get;
use function Wa72\Pyco\host;

class Database
{
    const TMP_FILE_PREFIX = 'temp_';
    protected static string $resultFileStructure = 'dump-structure.sql';
    protected static string $resultFileData = 'dump-data.sql';
    protected static string $resultFileStructureGz = 'dump-structure.sql.gz';
    protected static string $resultFileDataGz = 'dump-data.sql.gz';

    public static function export(string $host): array
    {
        $cmds = [];

        $dbStoragePath = host($host)
            ->get('db_storage_path');
        $hostalias = host($host)
            ->get('alias');

        $mysqlDumpBinary = host($host)->has('local/bin/mysqldump') ? host($host)->get('local/bin/mysqldump') : get('local/bin/mysqldump');
        $gzipBinary = host($host)->has('local/bin/gzip') ? host($host)->get('local/bin/gzip') : get('local/bin/gzip');

        $local = ('localhost' === $hostalias);

        $databaseConfig = host($host)->getMergedWithDefaults('database', 'db_default');

        $mysqlDumpArgs = [
            'password'            => $databaseConfig['password'],
            'local/bin/mysqldump' => $mysqlDumpBinary,
            'options'             => '',
            'host'                => escapeshellarg($databaseConfig['host']),
            'port'                => escapeshellarg($databaseConfig['port']),
//            'port'                => escapeshellarg((isset($databaseConfig['port']) && $databaseConfig['port']) ? $databaseConfig['port'] : 3306),
            'user'                => escapeshellarg($databaseConfig['user']),
            'dbname'              => escapeshellarg($databaseConfig['dbname']),
            'absolutePath'        => '', // --result-file
            'ignore-tables'       => ''
        ];

        $absolutePath = null;
//        $envFilePath = rtrim($absolutePath ?? getcwd(), DIRECTORY_SEPARATOR) . '/.env';
//
        $tmpPath = merge_paths($absolutePath ?? getcwd(), '.pyco');
        if (!is_dir($tmpPath)) {
            mkdir($tmpPath);
        }
        $dbloginFile = self::TMP_FILE_PREFIX . "dblogin.{$host}.cnf";

        // create file always locally
        $defaultsExtraFile = merge_paths($tmpPath, $dbloginFile);
        // possibly use escapeshellarg()?
        file_put_contents($defaultsExtraFile,
            sprintf("[client]\npassword='%s'\n", $mysqlDumpArgs['password'])
        );

        if (!$local) {
            $cmd = sprintf('scp %s %s@%s:%s',
                $defaultsExtraFile,
                host($host)->get('remote_user'),
                host($host)->get('hostname'),
                host($host)->get('db_storage_path')
            );
            $cmds[] = new Command($cmd, true);

            // use uploaded file
            $defaultsExtraFile = merge_paths(host($host)->get('db_storage_path'), $dbloginFile);
        }
        $mysqlDumpArgs['defaults-extra-file'] = '--defaults-extra-file=' . $defaultsExtraFile;

        $databaseUtility = new DatabaseUtility($databaseConfig['host'],
            $databaseConfig['user'],
            $databaseConfig['password'],
            $databaseConfig['dbname'],
            $databaseConfig['port']
        );

        if (isset($databaseConfig['ignore_tables_out']) && is_array($databaseConfig['ignore_tables_out'])) {
            // pyco doesn't support wildcard configuration
            // reason: no connection to foreign host database to use $databaseUtility->getTables() and filter them out.
//            $ignoreTables = ArrayHelper::filterWithRegexp(
//                $databaseConfig['ignore_tables_out'],
//                $databaseUtility->getTables()
//            );

            $ignoreTables = $databaseConfig['ignore_tables_out'];

            $ignore_tables_out = [];
            foreach ($ignoreTables as $item) {
                $ignore_tables_out[] = escapeshellarg($item);
            }
            $mysqlDumpArgs['ignore-tables'] = ArrayHelper::wrapEachItemAndImplode($ignore_tables_out,
                ' --ignore-table=' . $databaseConfig['dbname'] . '.');
        }


        // dump database structure
        $mysqlDumpArgs['options'] = get('db_export_mysqldump_options_structure', '--no-data --default-character-set=utf8 --no-tablespaces');
        $dumpCmd = sprintf(
            '%s %s %s -h%s -P%s -u%s %s -r%s %s',
            $mysqlDumpArgs['local/bin/mysqldump'],
            $mysqlDumpArgs['defaults-extra-file'],
            $mysqlDumpArgs['options'],
            $mysqlDumpArgs['host'],
            $mysqlDumpArgs['port'],
            $mysqlDumpArgs['user'],
            $mysqlDumpArgs['dbname'],
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileStructure)),
            $mysqlDumpArgs['ignore-tables']
        );
        $cmds[] = new Command($dumpCmd, $local);

        $cmd = sprintf('%s -f %s',
            $gzipBinary,
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileStructure))
        );
        $cmds[] = new Command($cmd, $local);

        // dump database data
        $mysqlDumpArgs['options'] = get('db_export_mysqldump_options_data', '--opt --skip-lock-tables --single-transaction --no-create-db --no-create-info --default-character-set=utf8 --no-tablespaces');
        $dumpCmd = sprintf(
            '%s %s %s -h%s -P%s -u%s %s -r%s %s',
            $mysqlDumpArgs['local/bin/mysqldump'],
            $mysqlDumpArgs['defaults-extra-file'],
            $mysqlDumpArgs['options'],
            $mysqlDumpArgs['host'],
            $mysqlDumpArgs['port'],
            $mysqlDumpArgs['user'],
            $mysqlDumpArgs['dbname'],
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileData)),
            $mysqlDumpArgs['ignore-tables'],
        );
        $cmds[] = new Command($dumpCmd, $local);

        $cmd = sprintf('%s -f %s',
            $gzipBinary,
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileData))
        );
        $cmds[] = new Command($cmd, $local);

        return $cmds;
    }

    public static function import(string $host): array
    {
        $cmds = [];

        $dbStoragePath = host($host)
            ->get('db_storage_path');
        $hostalias = host($host)
            ->get('alias');

        $mysqlBinary = host($host)->has('local/bin/mysql') ? host($host)->get('local/bin/mysql') : get('local/bin/mysql');
        $gzipBinary = host($host)->has('local/bin/gzip') ? host($host)->get('local/bin/gzip') : get('local/bin/gzip');

        $local = ('localhost' === $hostalias);

        $databaseConfig = host($host)->getMergedWithDefaults('database', 'db_default');

        $mysqlArgs = [
            'password'        => $databaseConfig['password'],
            'local/bin/mysql' => $mysqlBinary,
            'options'         => '',
            'host'            => escapeshellarg($databaseConfig['host']),
            'port'            => escapeshellarg((isset($databaseConfig['port']) && $databaseConfig['port']) ? $databaseConfig['port'] : 3306),
            'user'            => escapeshellarg($databaseConfig['user']),
            'dbname'          => escapeshellarg($databaseConfig['dbname']),
            'absolutePath'    => '', // --result-file
            'ignore-tables'   => ''
        ];

        $absolutePath = null;
//        $envFilePath = rtrim($absolutePath ?? getcwd(), DIRECTORY_SEPARATOR) . '/.env';
//
        $tmpPath = merge_paths($absolutePath ?? getcwd(), '.pyco');
        if (!is_dir($tmpPath)) {
            mkdir($tmpPath);
        }
        self::$resultFileStructure = 'dump-structure.sql';
        $resultFileData = 'dump-data.sql';
        $dbloginFile = self::TMP_FILE_PREFIX . "dblogin.{$host}.cnf";

        // create file always locally
        $defaultsExtraFile = merge_paths($tmpPath, $dbloginFile);
        // possibly use escapeshellarg()?
        file_put_contents($defaultsExtraFile,
            sprintf("[client]\npassword='%s'\n", $mysqlArgs['password'])
        );

        if (!$local) {
            $cmd = sprintf('scp %s %s@%s:%s',
                $defaultsExtraFile,
                host($host)->get('remote_user'),
                host($host)->get('hostname'),
                host($host)->get('db_storage_path')
            );
            $cmds[] = new Command($cmd, true);

            // use uploaded file
            $defaultsExtraFile = merge_paths(host($host)->get('db_storage_path'), $dbloginFile);
        }
        $mysqlArgs['defaults-extra-file'] = '--defaults-extra-file=' . $defaultsExtraFile;

        // import database structure
        $unzipCmd = sprintf('%s --stdout --uncompress %s',
            $gzipBinary,
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileStructureGz))
        );

        $mysqlArgs['options'] = get('db_import_mysql_options_structure', '');
        $dumpCmd = sprintf(
            '%s %s %s -h%s -P%s -u%s -D%s',
            $mysqlArgs['local/bin/mysql'],
            $mysqlArgs['defaults-extra-file'],
            $mysqlArgs['options'],
            $mysqlArgs['host'],
            $mysqlArgs['port'],
            $mysqlArgs['user'],
            $mysqlArgs['dbname'],
        );
        $cmds[] = new Command($unzipCmd . ' | ' . $dumpCmd, $local);

//        $cmd = sprintf('%s -f --uncompress %s',
//            $gzipBinary,
//            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileDataGz))
//        );
//        $cmds[] = new Command($cmd, $local);

        // import database data
        $unzipCmd = sprintf('%s --stdout --uncompress %s',
            $gzipBinary,
            escapeshellarg(merge_paths($dbStoragePath, self::$resultFileDataGz))
        );

        $mysqlArgs['options'] = get('db_import_mysql_options_data', '');
        $dumpCmd = sprintf(
            '%s %s %s -h%s -P%s -u%s -D%s',
            $mysqlArgs['local/bin/mysql'],
            $mysqlArgs['defaults-extra-file'],
            $mysqlArgs['options'],
            $mysqlArgs['host'],
            $mysqlArgs['port'],
            $mysqlArgs['user'],
            $mysqlArgs['dbname'],
        );
        $cmds[] = new Command($unzipCmd . ' | ' . $dumpCmd, $local);

        return $cmds;
    }

    public static function download(string $host): array
    {
        $cmds = [];

        $cmd = sprintf('scp %s@%s:%s %s',
            host($host)->get('remote_user'),
            host($host)->get('hostname'),
            escapeshellarg(merge_paths(host($host)->get('db_storage_path'), self::$resultFileStructureGz)),
            host('localhost')->get('db_storage_path')
        );
        $cmds[] = new Command($cmd, true);

        $cmd = sprintf('scp %s@%s:%s %s',
            host($host)->get('remote_user'),
            host($host)->get('hostname'),
            escapeshellarg(merge_paths(host($host)->get('db_storage_path'), self::$resultFileDataGz)),
            host('localhost')->get('db_storage_path')
        );
        $cmds[] = new Command($cmd, true);

        return $cmds;
    }

    public static function upload(string $host): array
    {
        $cmds = [];

        $cmd = sprintf('scp %s %s@%s:%s',
            escapeshellarg(merge_paths(host('localhost')->get('db_storage_path'), self::$resultFileStructureGz)),
            host($host)->get('remote_user'),
            host($host)->get('hostname'),
            host($host)->get('db_storage_path')
        );
        $cmds[] = new Command($cmd, true);

        $cmd = sprintf('scp %s %s@%s:%s',
            escapeshellarg(merge_paths(host('localhost')->get('db_storage_path'), self::$resultFileDataGz)),
            host($host)->get('remote_user'),
            host($host)->get('hostname'),
            host($host)->get('db_storage_path')
        );
        $cmds[] = new Command($cmd, true);

        return $cmds;
    }
}
