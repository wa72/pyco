<?php

namespace Wa72\Pyco\Command;

use Spatie\Ssh\Ssh;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Wa72\Pyco\Executor\SeriesExecutor;
use Wa72\Pyco\Task\Database;
use Wa72\Pyco\Task\Media;
use Wa72\Pyco\Utility\PycoFileFinder;

require_once __DIR__ . '/../sets/default/set-0.php';
//require_once __DIR__ . '/../sets/default/typo3v11.php';

class ClonePullCommand extends Command
{
    protected static $defaultName = 'clone-pull';
    protected static $defaultDescription = 'Download files and database to localhost.';

    const ARGUMENT_HOST = 'host';

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_HOST, InputArgument::REQUIRED, 'Source host.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $seriesExecutor = new SeriesExecutor($input, $output);

        $pycoFile = PycoFileFinder::find();

        $output->writeln("Using configuration from {$pycoFile}");
        $srcHost = $input->getArgument(self::ARGUMENT_HOST);
        $output->writeln("Cloning from {$srcHost}");

        require_once $pycoFile;

        $seriesExecutor->addCommands(Database::export($srcHost));
        $seriesExecutor->addCommands(Database::download($srcHost));
        $seriesExecutor->addCommands(Database::import('localhost'));
        $seriesExecutor->addCommands(Media::pull($srcHost));

        $seriesExecutor->run();


        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }

    protected function ex(
        SSh $ssh,
        InputInterface $input,
        OutputInterface $output
    ): string {
        $result = '';

        $uploadProcess = $ssh->upload('$localpatternsFile', '$destinationPatternsFile');
        if (!$uploadProcess->isSuccessful()) {
            $result .= 'Upload fails. ' . $uploadProcess->getExitCode() . ' : ' . $uploadProcess->getExitCodeText() . ' : ' . $uploadProcess->getErrorOutput();

            return $result;
        }

        // check dir
        $process = $ssh->execute('cd && ls -la html/typo3');
        if (!$process->isSuccessful()) {
            $result .= 'Change dir fails. ' . $process->getExitCode() . ' : ' . $process->getExitCodeText() . ' : ' . $process->getErrorOutput();

            return $result;
        }

        return $result;
    }
}
