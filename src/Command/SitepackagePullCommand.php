<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 24.03.2022
 * Time: 13:08
 */

namespace Wa72\Pyco\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Wa72\Pyco\Executor\SeriesExecutor;
use Wa72\Pyco\Task\Media;
use Wa72\Pyco\Utility\PycoFileFinder;

class SitepackagePullCommand extends Command
{
    protected static $defaultName = 'sitepackage-pull';
    protected static $defaultDescription = 'Download site package.';

    const ARGUMENT_HOST = 'host';

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_HOST, InputArgument::REQUIRED, 'Target host.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $pycoFile = PycoFileFinder::find();

        $output->writeln("Using configuration from {$pycoFile}.");
        $targetHost = $input->getArgument(self::ARGUMENT_HOST);
        $output->writeln("Pulling from {$targetHost}.");

        require_once $pycoFile;


        $seriesExecutor = new SeriesExecutor($input, $output);
        $seriesExecutor->addCommands(Media::pullSitepackage($targetHost));

        $seriesExecutor->run();

        return Command::SUCCESS;
    }
}
