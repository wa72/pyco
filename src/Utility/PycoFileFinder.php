<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 17.03.2022
 * Time: 13:14
 */

namespace Wa72\Pyco\Utility;

class PycoFileFinder
{
    public static function find(): string
    {
        $pycoFile = getcwd() . '/pyco.php';

        if (!is_readable($pycoFile)) {
            $currentDir = getcwd();
            $count = 0;
            do {
                $currentDir = dirname($currentDir);
                $pycoFile = $currentDir . '/pyco.php';
                $count++;
            } while (!is_readable($pycoFile) && $count < 100);
        }

        if (!is_readable($pycoFile)) {
            fwrite(
                STDERR,
                'pyco.php not found.' . PHP_EOL
            );
            exit(1);
        }

        return $pycoFile;
    }
}
