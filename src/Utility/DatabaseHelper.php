<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 17.03.2022
 * Time: 08:50
 */

namespace Wa72\Pyco\Utility;

class DatabaseUtility
{

    protected ?string $hostname = null;
    protected ?string $username = null;
    protected ?string $password = null;
    protected ?string $database = null;
    protected ?int $port = null;

    /**
     * @param string|null $hostname
     * @param string|null $username
     * @param string|null $password
     * @param string|null $database
     * @param int|null $port
     */
    public function __construct(?string $hostname, ?string $username, ?string $password, ?string $database, ?int $port)
    {
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->port = $port;
    }


    /**
     * @return array
     */
    public function getTables(): array
    {
        $allTables = [];

        $link = @mysqli_connect($this->hostname, $this->username, $this->password, $this->database, $this->port);
        if (!$link) {
            return $allTables;
        } elseif (!$link) {
            var_dump("mysqli_connect({$this->hostname}, {$this->username}, {$this->password}, {$this->database}, {$this->port});");
            die();
        }
        $result = $link->query('SHOW TABLES');

        while ($row = $result->fetch_row()) {
            $allTables[] = array_shift($row);
        }

        mysqli_close($link);

        return $allTables;
    }
}
