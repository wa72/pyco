<?php

namespace Wa72\Pyco;

use Wa72\Pyco\Configuration\Configuration;
use Wa72\Pyco\Configuration\Host;
use Wa72\Pyco\Task\Context;


function host(string $hostname)
{
    $host = Host::host($hostname);

    return $host;
}

/**
 * Merge new config params to existing config array.
 *
 * @param string $name
 * @param array $array
 */
function add(string $name, array $array)
{
    if (Context::has()) {
        $host = Context::getHost();
        $host->add($name, $array);
    } else {
        Configuration::addDefault($name, $array);
    }
}

/**
 * Setup configuration option.
 *
 * @param string $name
 * @param mixed $value
 */
function set(string $name, $value)
{
    Configuration::set($name, $value);
}


/**
 * Get configuration value.
 *
 * @param string $name
 * @param mixed|null $default
 * @return mixed
 */
function get(string $name, $default = null)
{
    if(Context::has()) {
        $host = Context::getHost();
        return $host->get($name, $default);
    } else {
        return Configuration::get($name, $default);
    }
}

function get_merged(string $collectionName, string $name, string $defaultName): array
{
    return Configuration::getMergedWithDefaults($collectionName, $name, $defaultName);
}

/**
 * Overrides values in $override with values from $original if present.
 *
 * Recursively merge two config arrays with a specific behavior:
 *
 * 1. scalar values are overridden
 * 2. array values are extended uniquely if all keys are numeric
 * 3. all other array values are merged
 *
 * @param array $original   Can be overridden.
 * @param array $override
 * @return array
 * @see http://stackoverflow.com/a/36366886/6812729
 */
function array_merge_alternate(array $original, array $override): array
{
    foreach ($override as $key => $value) {
        if (isset($original[$key])) {
           if (!is_array($original[$key])) {
                if (is_numeric($key)) {
                    // Append scalar value
                    $original[] = $value;
                } else {
                    // Override scalar value
                    $original[$key] = $value;
                }
            } elseif (array_keys($original[$key]) === range(0, count($original[$key]) - 1)) {
                // Uniquely append to array with numeric keys
                $original[$key] = array_unique(array_merge($original[$key], $value));
            } else {
                // Merge all other arrays
                $original[$key] = array_merge_alternate($original[$key], $value);
            }
        } else {
            // Simply addDefault new key/value
            $original[$key] = $value;
        }
    }

    return $original;
}
