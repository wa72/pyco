#!/usr/bin/env bash
#
# deploy und retrieve angelehnt an: http://rocketeer.autopergamene.eu/II-Concepts/Connections-and-Stages.html
#

# generic helper
confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

# project upgrade

## alles nun in theme Verzeichnis, was zum build gebraucht wird.
if [ ! -e "./theme/package.json" ]; then
    confirm "Upgrade theme directory? [y/N]"
    retval=$?
    if [ "$retval" == 0 ]
    then
        mv ./node_modules/ ./theme/
        mv ./package.json ./theme/
        mv ./postcss.config.js ./theme/
        mv ./webpack.config.js ./theme/
        mv ./yarn.lock ./theme/

        if [ -d "./theme/src_base_schulen" ]; then
            cp ./pystanium/bootstrap/schulhomepage/theme/webpack.config.js ./theme/webpack.config.js
        else
            cp ./pystanium/bootstrap/common/theme/webpack.config.js ./theme/webpack.config.js
        fi 
    fi
fi 

## pystanium/console/ ist nun pystanium/pyco/
if [ -d "./pystanium/console" ]; then
    confirm "Upgrade pystanium/console to pystanium/pyco? [y/N]"
    retval=$?
    if [ "$retval" == 0 ]
    then
        rm -rf ./pystanium/console

        cd pystanium/pyco/ ; composer install ; cd ../..
    fi
fi 


BASEDIR="$PWD"
TYPO3VERSION=7
DRY_RUN=

SCP_OPTIONS="-p -q -o stricthostkeychecking=no"
SSH_OPTIONS="-T -o StrictHostKeyChecking=no"
RSYNC_SSH_OPTIONS="-o StrictHostKeyChecking=no"
SSHPASSCMD="sshpass -e"

STRATEGY=update

EXCLUDED_TABLES=('cache_pages' 'cache_extensions' 'cache_hash' 'link_cache' 'cache_typo3temp_log' 'cache_imagesizes' 'cache_md5params' 'cache_pages' 'cache_pagesection' 'cache_treelist' 'cachingframework_cache_hash'
'cachingframework_cache_hash_tags' 'cachingframework_cache_pages' 'cachingframework_cache_pages_tags' 'cachingframework_cache_pagesection' 'cachingframework_cache_pagesection_tags'
'be_sessions' 'fe_session_data' 'fe_sessions' 'index_fulltext' 'index_grlist' 'index_phash' 'index_rel' 'index_section' 'index_stat_search' 'index_stat_word' 'index_words' 'index_debug'
'cf_cache_hash' 'cf_cache_hash_tags' 'cf_cache_news_category' 'cf_cache_news_category_tags' 'cf_cache_pages' 'cf_cache_pagesection' 'cf_cache_pagesection_tags' 'cf_cache_pages_tags' 'cf_cache_rootline'
'cf_cache_rootline_tags' 'cf_extbase_datamapfactory_datamap' 'cf_extbase_datamapfactory_datamap_tags' 'cf_extbase_object' 'cf_extbase_object_tags' 'cf_extbase_reflection' 'cf_extbase_reflection_tags'
'cf_extbase_typo3dbbackend_queries' 'cf_extbase_typo3dbbackend_queries_tags' 'cf_extbase_typo3dbbackend_tablecolumns' 'cf_extbase_typo3dbbackend_tablecolumns_tags' 'cf_fluidcontent' 'cf_fluidcontent_tags'
'cf_flux' 'cf_flux_tags' 'cf_vhs_main' 'cf_vhs_main_tags' 'cf_vhs_markdown' 'cf_vhs_markdown_tags' 'link_cache' 'link_oldlinks' 'tt_news_cache' 'sys_history' 'sys_log' 'sys_dmail_maillog')

cd "$( dirname "${BASH_SOURCE[0]}" )"

source ./pystanium.conf

CMD=$1
shift
while [ $# -gt 0 ]; do
  case "$1" in
    --dbname=*)
        DBNAME="${1#*=}"
      ;;
    --typo3version=*)
        TYPO3VERSION="${1#*=}"
      ;;
    --on=*)
        CONNECTIONNAME="${1#*=}"
      ;;
    --from=*)
        CONNECTIONNAME="${1#*=}"
      ;;
    --strategy=*)
        STRATEGY="${1#*=}"
        if ! [[ $STRATEGY == 'update' || $STRATEGY == 'clone' || $STRATEGY == 'theme' || $STRATEGY == 'extensions' || $STRATEGY == 'mask' ]] ; then
            printf "Error: unknown strategy $STRATEGY.\n"
            exit 1    
        fi
        ;;
    --dry-run)
        DRY_RUN=--dry-run
      ;;
    *)
      printf "***************************\n"
      printf "* Error: Invalid argument $1 .*\n"
      printf "***************************\n"
      exit 1
  esac
  shift
done


# Aufbereiten und prüfen der Einstellungen aus pystanium.conf
# REMOTE_HTDOCS_PATH
# REMOTE_DB
# REMOTE_DB_USER
# REMOTE_DB_PASSWORD
# REMOTE_DB_HOST
# REMOTE_TMP_PATH
function processConf {
    # bash Variablen sollen in Großbuchstaben sein.
    CONNECTIONNAMEUC="${CONNECTIONNAME^^}"

    # Wenn connectionname angegeben (--on= --from=), dann versuchen die connection zu bekommen.
    if [[ ! -z ${CONNECTIONNAMEUC} ]]; then
        # connection besteht aus login@hostname
        declare "TMPVARNAME=${CONNECTIONNAMEUC}_HOST"
        declare "TMPVARNAME2=${CONNECTIONNAMEUC}_LOGIN_NAME"
        declare "TMPVARNAME3=${CONNECTIONNAMEUC}_PASSWORD"

        # prüfen, ob diese in der conf vorhanden sind
        if [[ -z ${!TMPVARNAME} ]]; then
            echo Fehlende Konfiguration in pystanium.conf: $TMPVARNAME
            exit 1
        fi
        if [[ -z ${!TMPVARNAME2} ]]; then
            echo Fehlende Konfiguration in pystanium.conf: $TMPVARNAME2
            exit 1
        fi
        if [[ -z ${!TMPVARNAME3} ]]; then
            # kein ssh_pass sondern ssh-key ist auf server hinterlegt.
            SSHPASSCMD=""
            TMPVARNAME3=""
            # echo Fehlende Konfiguration in pystanium.conf: $TMPVARNAME3
            # exit 1
        else
            REMOTE_PASSWORD="${!TMPVARNAME3}"
            # SSHPASSCMD verwendet dies
            export SSHPASS="${REMOTE_PASSWORD}"
        fi        

        REMOTE_HOST="${!TMPVARNAME}"
        REMOTE_LOGIN_NAME="${!TMPVARNAME2}"
    
        CONNECTION="$REMOTE_LOGIN_NAME@$REMOTE_HOST"
        if [[ -z ${CONNECTION} ]]; then
            echo No such connection specified: $CONNECTIONNAME
            exit 1
        fi
    fi

    declare "TMPVARNAME=${CONNECTIONNAMEUC}_HTDOCS_PATH"
    REMOTE_HTDOCS_PATH=${!TMPVARNAME}
    declare "TMPVARNAME=${CONNECTIONNAMEUC}_DB"
    REMOTE_DB=${!TMPVARNAME}
    declare "TMPVARNAME=${CONNECTIONNAMEUC}_DB_USER"
    REMOTE_DB_USER=${!TMPVARNAME}
    declare "TMPVARNAME=${CONNECTIONNAMEUC}_DB_PASSWORD"
    REMOTE_DB_PASSWORD=${!TMPVARNAME}
    declare "TMPVARNAME=${CONNECTIONNAMEUC}_DB_HOST"
    REMOTE_DB_HOST=${!TMPVARNAME}
    declare "TMPVARNAME=${CONNECTIONNAMEUC}_TMP_PATH"
    REMOTE_TMP_PATH=${!TMPVARNAME}

    if [ -d "./htdocs/typo3conf/ext" ]; then
        TYPO3_PROJECT=1
        LOCAL_HTDOCS_PATH="./htdocs"
    elif [ -d "./typo3cms/public" ]; then
        TYPO3_PROJECT=1
        LOCAL_HTDOCS_PATH="./typo3cms/public"
    else
        TYPO3_PROJECT=
    fi

    # raten, ob es ein Mittwald-Server ist
    if [[ $REMOTE_DB_HOST =~ .*\.mydbserver\.com.* ]]; then
        MITTWALD_SERVER=1
    else
        MITTWALD_SERVER=0
    fi

    IGNORED_TABLES_STRING=''
    for TABLE in "${EXCLUDED_TABLES[@]}"
    do :
        IGNORED_TABLES_STRING+=" --ignore-table=${LOCAL_DB}.${TABLE}"
    done
}

function check_status {
    if [ "$1" -eq "0" ]; then
        return;
    fi

    tput setf 4
    printf 'Problemchen... [%s]' "$2"
    tput sgr 0
    printf "\n"

    exit 1;
}

# Erwartet als Parameter einen String mit einem command je Zeile.
# Dieser String wird ggfs. als Fehlermeldung ausgegeben.
function execute_commands {
    COMMAND_STRING=$1
    mapfile -t COMMANDS <<< "$COMMAND_STRING"
    for COMMAND in "${COMMANDS[@]}"
        do :
            eval ${COMMAND}
            check_status $? "${COMMAND}"
        done
}

# erwartet als Parameter einen String mit einem command je Zeile.
function execute_remote_commands {
    REMOTE_SERVER=$1
    COMMAND_STRING=$2
    mapfile -t COMMANDS <<< "$COMMAND_STRING"

    REMOTE_COMMANDS_STRING='function check_status { if [ "$?" -ne "0" ] ; then exit 1; fi }'
    for COMMAND in "${COMMANDS[@]}"
        do :
            if [[ ! -z "${COMMAND// }" ]]; then
                if [[ ! -z "${REMOTE_COMMANDS_STRING// }" ]]; then
                    REMOTE_COMMANDS_STRING+=' ; '
                fi
                REMOTE_COMMANDS_STRING+="${COMMAND}"
                REMOTE_COMMANDS_STRING+=' ; check_status'
            fi
        done
    $SSHPASSCMD \
        ssh ${SSH_OPTIONS} ${REMOTE_SERVER} ${REMOTE_COMMANDS_STRING}

    check_status $? "ssh ${SSH_OPTIONS} ${REMOTE_SERVER} ${REMOTE_COMMANDS_STRING}"
}

function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }


function webpack_version {
    version=$(./theme/node_modules/.bin/webpack -v)
    

    # if [ $(version $version) -le $(version "3.0.0") ]; then
    #     echo "Version $version is < 3"
    # fi

    if [ $(version $version) -ge $(version $1) ]; then
        # echo "Version ${version} is > ${1}"
        echo 1
    else 
        echo 0
    fi
}

function serve {
    # webpack >= 4?
    v=$(webpack_version '4.0.0')

    cd theme
    if [ $v -eq 1 ]
    then
        ./node_modules/.bin/webpack-dev-server --mode=development 
    else
        WEBPACK_MODE=dev ./node_modules/.bin/webpack-dev-server 
    fi

    cd "$BASEDIR"
}

function build {
    if [ ! -d "./theme" ]; then
        echo "No theme, no build."
        return
    fi

    processConf

    # webpack >= 4?
    v=$(webpack_version '4.0.0')

    cd theme
    if [ $v -eq 1 ]
    then
        ./node_modules/.bin/webpack --mode=production
    else
        WEBPACK_MODE=prod ./node_modules/.bin/webpack -p
    fi
    cd "$BASEDIR"

    if [ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Css" ]; then
        echo "copy -> ${LOCAL_HTDOCS_PATH}/*/Resources/Public/Css"
        cp -a ./theme/build/assets/*.css ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Css 2>/dev/null
    fi
    if [ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/JavaScript" ]; then
        echo "copy -> ${LOCAL_HTDOCS_PATH}/*/Resources/Public/JavaScript"
        cp -a ./theme/build/assets/*.js ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/JavaScript 2>/dev/null
    fi
    if [ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Images" ]; then
        echo "copy -> ${LOCAL_HTDOCS_PATH}/*/Resources/Public/Images"
        cp -af ./theme/build/assets/*.jpg ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Images 2>/dev/null
        cp -af ./theme/build/assets/*.png ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Images 2>/dev/null
        cp -af ./theme/build/assets/*.svg ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Images 2>/dev/null
        cp -af ./theme/build/assets/*.gif ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Images 2>/dev/null
    fi
    if [ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Assets" ]; then
        echo "rsync -> ${LOCAL_HTDOCS_PATH}/*/Resources/Public/Assets"
        rsync -a -q --checksum  --delete --exclude-from=rsync_exclude.txt ./theme/build/assets/ ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Public/Assets 
        echo "cp    -> ${LOCAL_HTDOCS_PATH}/*/Resources/Private/Layouts"
        cp -a ./theme/build/index.html ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Resources/Private/Layouts/Page/DefaultLayout.raw.html 
        php ${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site/Classes/Console/cleanup.php
    fi
    
}
function generate_pages {
    if [ -d "./pagegenerator" ]; 
    then
        php pagegenerator/console.php pa:ge --rsync -vvv
    fi
}



function deployDatabase {
    if [[ ${STRATEGY} != 'clone' ]]; then
        echo 'Skip deploy database ' ${REMOTE_DB} ' for strategy: ' ${STRATEGY}

        return
    fi

    if [[ -n ${DRY_RUN} ]]; then
            echo 'Skip deploy database ' ${REMOTE_DB}
            return
    fi

    processConf

    FILE_STRUCTURE=${LOCAL_DB}-structure.sql
    FILE_DATA=${LOCAL_DB}-data.sql

    echo 'Dump local database' ${LOCAL_DB} 'and transfer'
    execute_commands "
sshpass -p '${LOCAL_DB_PASSWORD}' mysqldump -d ${LOCAL_DB} -u ${LOCAL_DB_USER} -p -h ${LOCAL_DB_HOST} > ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}
gzip -f ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}
sshpass -p '${LOCAL_DB_PASSWORD}' mysqldump -nt ${LOCAL_DB} -u ${LOCAL_DB_USER} -p -h ${LOCAL_DB_HOST} ${IGNORED_TABLES_STRING} > ${LOCAL_TMP_PATH}/${FILE_DATA}
gzip -f ${LOCAL_TMP_PATH}/${FILE_DATA}
$SSHPASSCMD \
    scp $SCP_OPTIONS ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}.gz ${CONNECTION}:${REMOTE_TMP_PATH}/
$SSHPASSCMD \
    scp $SCP_OPTIONS ${LOCAL_TMP_PATH}/${FILE_DATA}.gz ${CONNECTION}:${REMOTE_TMP_PATH}/
"

CLEANED_PASSWORD=${REMOTE_DB_PASSWORD//%/%%}
echo 'Import database ' ${REMOTE_DB}
execute_remote_commands ${CONNECTION} "
gunzip -f ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}.gz
gunzip -f ${REMOTE_TMP_PATH}/${FILE_DATA}.gz

printf \"[client]\npassword='${CLEANED_PASSWORD}'\n\" >${REMOTE_TMP_PATH}/dblogin.cnf   

mysql --defaults-extra-file=${REMOTE_TMP_PATH}/dblogin.cnf ${REMOTE_DB} -u ${REMOTE_DB_USER}  -h ${REMOTE_DB_HOST} < ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}
mysql --defaults-extra-file=${REMOTE_TMP_PATH}/dblogin.cnf ${REMOTE_DB} -u ${REMOTE_DB_USER}  -h ${REMOTE_DB_HOST} < ${REMOTE_TMP_PATH}/${FILE_DATA}

rm ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}
rm ${REMOTE_TMP_PATH}/${FILE_DATA} 
rm ${REMOTE_TMP_PATH}/dblogin.cnf
"



    execute_commands "
rm ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}.gz
rm ${LOCAL_TMP_PATH}/${FILE_DATA}.gz
"

}

function deploy {
    processConf

    if [[ $STRATEGY == 'clone' ]]; then
        confirm "Clone to ${CONNECTION}? [y/N]"  || return
    fi

    if [[ $STRATEGY == 'theme' && $CONNECTIONNAME == 'preview' ]]; then
        echo Deploying theme preview from /theme/build to $CONNECTIONNAME: $CONNECTION
        $SSHPASSCMD \
            rsync -a --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
            ./theme/build/ $CONNECTION:${REMOTE_HTDOCS_PATH}/ $DRY_RUN
    elif [[ $TYPO3_PROJECT ]]; then
        echo Deploying to $CONNECTIONNAME: $CONNECTION

        if [[ $STRATEGY == 'mask' ]]; then
            if [[ -d "${LOCAL_HTDOCS_PATH}/fileadmin/templates" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ./${LOCAL_HTDOCS_PATH}/fileadmin/templates/ $CONNECTION:${REMOTE_HTDOCS_PATH}/fileadmin/templates/
            fi
        elif [[ $STRATEGY == 'extensions' ]]; then
            if [[ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ./htdocs/typo3conf/ext/ $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/ext/
            fi
        elif [[ $STRATEGY == 'update' ]]; then

            if [[ -d "${LOCAL_HTDOCS_PATH}/fileadmin/wa72" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ./htdocs/fileadmin/wa72/ $CONNECTION:${REMOTE_HTDOCS_PATH}/fileadmin/wa72/
            fi

            if [[ -d "${LOCAL_HTDOCS_PATH}/typo3conf/ext/user_site" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ./htdocs/typo3conf/ext/user_site/ $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/ext/user_site/
            fi

            if [[ -d "${LOCAL_HTDOCS_PATH}/typo3conf/sites" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ./htdocs/typo3conf/sites/ $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/sites/
            fi

        elif [[ $STRATEGY == 'clone' ]]; then
            deployDatabase

            $SSHPASSCMD \
                rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                ${LOCAL_HTDOCS_PATH}/typo3conf/ext/ $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/ext/

            if [[ -d "${LOCAL_HTDOCS_PATH}/typo3conf/sites" ]]; then
                $SSHPASSCMD \
                    rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                    ${LOCAL_HTDOCS_PATH}/typo3conf/sites/ $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/sites/
            fi

            $SSHPASSCMD \
                rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                ${LOCAL_HTDOCS_PATH}/fileadmin/ $CONNECTION:${REMOTE_HTDOCS_PATH}/fileadmin/

            $SSHPASSCMD \
                rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
                ${LOCAL_HTDOCS_PATH}/uploads/ $CONNECTION:${REMOTE_HTDOCS_PATH}/uploads/

            $SSHPASSCMD \
                scp $SCP_OPTIONS ${LOCAL_HTDOCS_PATH}/typo3conf/AdditionalConfiguration.php $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/AdditionalConfiguration.php 2> /dev/null
        else
            echo 'Unknown for strategy: ' ${STRATEGY}

            return;
        fi

        if [[ $MITTWALD_SERVER ]]; then
            $SSHPASSCMD \
                    scp $SCP_OPTIONS ./pystanium/bootstrap/mittwald/etc/php/* $CONNECTION:/etc/php/ 2> /dev/null
        fi

#         execute_remote_commands ${CONNECTION} "
# bash ${REMOTE_HTDOCS_PATH}/typo3conf/ext/user_site/Classes/Console/cleanup.php
# "
    else
        echo Deploying to $CONNECTIONNAME: $CONNECTION
        # $SSHPASSCMD \
        #   rsync -a --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
        #   ./htdocs/ $CONNECTION $DRY_RUN
    fi
}

function retrieveDatabase {
    processConf

    if [[ -n ${DRY_RUN} ]]; then
        echo 'Skip retrieve database ' ${REMOTE_DB}
        return
    fi


    FILE_STRUCTURE=${LOCAL_DB}-structure.sql
    FILE_DATA=${LOCAL_DB}-data.sql

# printf '[client]\npassword=''"'"'${REMOTE_DB_PASSWORD}'"'"'' >${REMOTE_TMP_PATH}/dblogin.cnf

    # % ersetzen für printf
    CLEANED_PASSWORD=${REMOTE_DB_PASSWORD//%/%%}
    echo 'Dump remote database ' ${REMOTE_DB}
    execute_remote_commands ${CONNECTION} "
printf \"[client]\npassword='${CLEANED_PASSWORD}'\n\" >${REMOTE_TMP_PATH}/dblogin.cnf   

mysqldump --defaults-extra-file=${REMOTE_TMP_PATH}/dblogin.cnf -d ${REMOTE_DB} -u ${REMOTE_DB_USER} -h ${REMOTE_DB_HOST} > ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}
gzip -f ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}
mysqldump --defaults-extra-file=${REMOTE_TMP_PATH}/dblogin.cnf -nt ${REMOTE_DB} -u ${REMOTE_DB_USER} -h ${REMOTE_DB_HOST} ${IGNORED_TABLES_STRING} > ${REMOTE_TMP_PATH}/${FILE_DATA}
gzip -f ${REMOTE_TMP_PATH}/${FILE_DATA}
"

    echo 'Transfer and import database ' ${LOCAL_DB}
    execute_commands "
$SSHPASSCMD  scp $SCP_OPTIONS ${CONNECTION}:${REMOTE_TMP_PATH}/${FILE_STRUCTURE}.gz ${LOCAL_TMP_PATH}/
$SSHPASSCMD  scp $SCP_OPTIONS ${CONNECTION}:${REMOTE_TMP_PATH}/${FILE_DATA}.gz ${LOCAL_TMP_PATH}/

gunzip -f ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}.gz
gunzip -f ${LOCAL_TMP_PATH}/${FILE_DATA}.gz

sshpass -p "${LOCAL_DB_PASSWORD}" mysql ${LOCAL_DB} -u ${LOCAL_DB_USER} -p -h ${LOCAL_DB_HOST} < ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}
sshpass -p "${LOCAL_DB_PASSWORD}" mysql ${LOCAL_DB} -u ${LOCAL_DB_USER} -p -h ${LOCAL_DB_HOST} < ${LOCAL_TMP_PATH}/${FILE_DATA}

#mysql ${LOCAL_DB} -u ${LOCAL_DB_USER} -p'${LOCAL_DB_PASSWORD}' -h ${LOCAL_DB_HOST} < ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}
#mysql ${LOCAL_DB} -u ${LOCAL_DB_USER} -p'${LOCAL_DB_PASSWORD}' -h ${LOCAL_DB_HOST} < ${LOCAL_TMP_PATH}/${FILE_DATA}


rm ${LOCAL_TMP_PATH}/${FILE_STRUCTURE}
rm ${LOCAL_TMP_PATH}/${FILE_DATA} 
"

    execute_remote_commands ${CONNECTION} "
rm ${REMOTE_TMP_PATH}/${FILE_STRUCTURE}.gz
rm ${REMOTE_TMP_PATH}/${FILE_DATA}.gz
rm ${REMOTE_TMP_PATH}/dblogin.cnf
"

}

function retrieve {
    processConf

    if ! [[ $STRATEGY == 'update' ]]; then
        printf "Error: unknown strategy $STRATEGY.\n"
        exit 1
    fi

    if [[ TYPO3_PROJECT ]]; then
        echo Retrieving from $CONNECTIONNAME: $CONNECTION
        retrieveDatabase

        if ! [[ $DRY_RUN ]]; then
            $SSHPASSCMD \
                scp $SCP_OPTIONS $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/real_url.php ${LOCAL_HTDOCS_PATH}/typo3conf/LocalConfiguration.php.${CONNECTIONNAME^^} 2> /dev/null
            $SSHPASSCMD \
                scp $SCP_OPTIONS $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/LocalConfiguration.php ${LOCAL_HTDOCS_PATH}/typo3conf/LocalConfiguration.php.${CONNECTIONNAME^^} 2> /dev/null
            $SSHPASSCMD \
                scp $SCP_OPTIONS $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/PackageStates.php ${LOCAL_HTDOCS_PATH}/typo3conf/PackageStates.php.${CONNECTIONNAME^^} 2> /dev/null
            $SSHPASSCMD \
                scp $SCP_OPTIONS $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/AdditionalConfiguration.php ${LOCAL_HTDOCS_PATH}/typo3conf/AdditionalConfiguration.php.${CONNECTIONNAME^^} 2> /dev/null
        fi

        $SSHPASSCMD \
            rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt --exclude 'user_site' -e "ssh $RSYNC_SSH_OPTIONS " \
            $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/ext/ ${LOCAL_HTDOCS_PATH}/typo3conf/ext/

        # Typo3 9: Ordner typo3conf/sites enthält Konfiguration für neues RealURL-Ersatz-System
        $SSHPASSCMD \
            rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
            $CONNECTION:${REMOTE_HTDOCS_PATH}/typo3conf/sites/ ${LOCAL_HTDOCS_PATH}/typo3conf/sites/

        $SSHPASSCMD \
            rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt --exclude '_processed_' --exclude '_temp_' -e "ssh $RSYNC_SSH_OPTIONS " \
            $CONNECTION:${REMOTE_HTDOCS_PATH}/fileadmin/ ${LOCAL_HTDOCS_PATH}/fileadmin/

        $SSHPASSCMD \
            rsync $DRY_RUN -avu --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
            $CONNECTION:${REMOTE_HTDOCS_PATH}/uploads/ ${LOCAL_HTDOCS_PATH}/uploads/
    else
        echo Retrieving from $CONNECTIONNAME: $CONNECTION
        # $SSHPASSCMD \
        #   rsync -a --checksum  --delete --exclude-from=rsync_exclude.txt -e "ssh $RSYNC_SSH_OPTIONS " \
        #   ./htdocs/ $CONNECTION $DRY_RUN
    fi
}

case "$CMD" in
    serve)
        serve
    ;;
    build)
        build
    ;;
    gp)
        build
        generate_pages
    ;;
    deploy)
        build
        generate_pages
        deploy
    ;;
    retrieve)
        retrieve
    ;;
    *)
        echo "Unbekanntes Kommando $CMD"
        exit 1
    ;;
esac

exit 0



if [[ -z "$DBNAME" ]]; then
	echo "Datenbankname fehlt. --dbname=foodb"
    exit 1
fi

# if [[ -z "$1" ]]; then
# 	echo "Datenbankname fehlt"
#     exit 1
# fi
# DBNAME=$1

if [ ! -d "../htdocs" ]; then
	echo "Kein Verzeichnis ../htdocs. Aufruf muss im dev-Verzeichnis erfolgen."
	exit 1
fi

if [ -d "../htdocs/typo3_src" ]; then
  echo "TYPO3 CMS schon installiert?"
  exit 1
fi

