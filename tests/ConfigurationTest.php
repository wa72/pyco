<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wa72\Pyco\Configuration\Configuration;
use Wa72\Pyco\Configuration\Host;
use function Wa72\Pyco\get;
use function Wa72\Pyco\set;

class ConfigurationTest extends TestCase
{

    public function testRealExample()
    {
        Configuration::addDefault('database-defaults',
            [
                'port' => '3306',
            ]
        );

        Configuration::addDefault('mittwald-defaults',
            [
                '/usr/bin/php' => '/usr/local/bin/php_cli',
            ]
        );

        Host::host('customer')
            ->setRealHostname('www.customer.org')
            ->add('database',
                [
                    'dbname' => 'db12345',
                    'port' => '6666',
                ]
            );

        $r = Host::host('customer')->getMergedWithDefaults('database','database-defaults');
        $this->assertEquals('db12345', $r['dbname']);
    }

    public function testGetMerged()
    {
        Configuration::addDefault('host-settings-defaults',
            [
                'url' => 'unknown.org',
                'type' => 'Web CMS',
            ]
        );
        $this->assertTrue(Configuration::has('host-settings-defaults'));
        $this->assertSame(2, count(Configuration::get('host-settings-defaults')));

        Configuration::add('host-settings',
            [
                'url' => 'typo3.com'
            ],
            'project',
            'typo3'
        );

        Configuration::add('host-settings',
            [
                'url' => 'symfony.com',
                'type' => 'PHP Framework',
            ],
            'project',
            'symfony'
        );

        Configuration::add('host-settings',
            [
                'logo-color' => 'red',
            ],
            'project',
            'none'
        );

        $configMerged = Configuration::getMergedWithDefaults('host-settings',  'host-settings-defaults', 'project', 'none');
        $this->assertSame('unknown.org', $configMerged['url']);
        $this->assertSame('Web CMS', $configMerged['type']);
        $this->assertSame('red', $configMerged['logo-color']);

        $configTypo3 = Configuration::getMergedWithDefaults('host-settings',  'host-settings-defaults', 'project', 'typo3',);


        $this->assertSame('typo3.com', $configTypo3['url']);
        $this->assertSame('Web CMS', $configTypo3['type']);

        $configMerged = Configuration::getMergedWithDefaults('host-settings',  'host-settings-defaults', 'project', 'symfony');
        $this->assertSame('symfony.com', $configMerged['url']);
        $this->assertSame('PHP Framework', $configMerged['type']);

    }

    public function testSet()
    {
        Configuration::set('set-simple', 0);
        $this->assertSame(0, Configuration::get('set-simple'));

        Configuration::set('set-simple', 'abc');
        $this->assertSame('abc', Configuration::get('set-simple'));

        Configuration::set('set-simple2', 'hello');
        $this->assertSame('hello', Configuration::get('set-simple2'));

        Configuration::set('set-date', new \DateTime('2020-01-01 00:00:01'));
        $this->assertSame('2020-01-01 00:00:01', Configuration::get('set-date')->format('Y-m-d H:i:s'));

        Configuration::set('set-array', [5, 6, 7]);
        $a = Configuration::get('set-array');
        $this->assertIsArray($a);
        $this->assertCount(3, $a);
        $this->assertSame(5, $a[0]);
        $this->assertSame(6, $a[1]);
        $this->assertSame(7, $a[2]);
    }

    public function testSetSubConfigurations()
    {
        Configuration::set('set-simple', 0);
        $this->assertSame(0, Configuration::get('set-simple'));

        Configuration::set('url', 'typo3.com', 'project', 'typo3');
        Configuration::set('url', 'symfony.com', 'project', 'symfony');
        $this->assertSame('typo3.com', Configuration::get('url', null, 'project', 'typo3'));
        $this->assertSame('symfony.com', Configuration::get('url', null, 'project', 'symfony'));
        $this->assertSame(0, Configuration::get('set-simple'));

        Configuration::set('set-simple2', 'hello');
        $this->assertSame('hello', Configuration::get('set-simple2'));
    }

    public function testAddDefault()
    {
        $this->assertFalse(Configuration::has('default-dummy'));
        Configuration::addDefault('default-dummy', ['red', 'blue']);
        $this->assertTrue(Configuration::has('default-dummy'));
    }

    public function testGetWithoutDefault()
    {
        $this->expectExceptionMessage('Configuration parameter `dummy-exception` does not exist.');
        get('dummy-exception');
    }

    public function testGet()
    {
        $this->assertSame('bar', get('dummy-get', 'bar'));

        set('dummy-get', 'bar2');
        $this->assertSame('bar2', get('dummy-get'));

    }

    public function testHas()
    {
        $this->assertFalse(Configuration::has('dummy'));
        set('dummy', 'bar');
        $this->assertTrue(Configuration::has('dummy'));
    }
}
